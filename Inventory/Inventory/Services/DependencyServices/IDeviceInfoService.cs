﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Services.DependencyServices
{
    /// <summary>
    /// Interface permettant la récupération d'informations du matériel
    /// </summary>
    public interface IDeviceInfoService
    {
        string GetIdentifier();
    }
}
