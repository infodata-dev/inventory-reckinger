﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Services.DependencyServices
{
    public interface ISqLiteService
    {
        SQLiteConnection GetConnexion(string databaseName);
        SQLiteAsyncConnection GetAsyncConnexion(string databaseName);
    }
}
