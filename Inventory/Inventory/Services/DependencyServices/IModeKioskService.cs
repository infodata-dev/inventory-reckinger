﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Services.DependencyServices
{
    /// <summary>
    /// Interface permettant de switcher le mode kiosk
    /// </summary>
    public interface IModeKioskService
    {
        void SwitchModeKiosk();
        bool GetSwitchModeKiosk();
    }
}
