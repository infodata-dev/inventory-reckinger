﻿using Inventory.Models;
using Inventory.Services.Web;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Inventory.Services.DependencyServices
{
	public interface IUpdateService
	{
		Task<WSResult> DownloadAndUpdateApp(CancellationToken token, string apkPath);
	}
}
