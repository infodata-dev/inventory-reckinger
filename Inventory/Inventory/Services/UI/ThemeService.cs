﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Inventory.Resources;

namespace Inventory.Services.UI
{
    public static class UIService
    {
        /// <summary>
        /// Regroupe les différents thèmes de l'application
        /// </summary>
        private static Dictionary<string, ResourceDictionary> themes =
            new Dictionary<string, ResourceDictionary>() {
                { "Dark", new DarkTheme() },
                { "Light", new LightTheme() }};

        /// <summary>
        /// Applique le thème désigné
        /// </summary>
        public static bool SetTheme(string theme)
        {
            if (!themes.ContainsKey(theme))
                return false;

            try
            {
                Application.Current.Resources.Clear();
                Application.Current.Resources.Add(new Styles());
                Application.Current.Resources.Add(themes[theme]);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Style GetStyle(string style, Type t)
        {
            Styles c = new Styles();
            Object obj = null;

            if (c.TryGetValue(style, out obj))
            {
                return (Style)obj;
            }

            foreach (ResourceDictionary rd in Application.Current.Resources.MergedDictionaries)
            {
                if (rd.TryGetValue(style, out obj))
                {
                    return (Style)obj;
                }
                else
                {
                    continue;
                }
            }

            return new Style(t);
        }

        public static Color GetColor(string color)
        {
            Colors c = new Colors();
            Object obj = null;

            if (c.TryGetValue(color, out obj))
            {
                return (Color)obj;
            }

            foreach (ResourceDictionary rd in Application.Current.Resources.MergedDictionaries)
            {
                if (rd.TryGetValue(color, out obj))
                {
                    return (Color)obj;
                }
                else
                {
                    continue;
                }
            }

            return Color.White;
        }
    }
}
