﻿using System;
using System.Collections.Generic;
using System.Text;
using Inventory.Services.Database;
using Inventory.Services.Session;
using Inventory.Services.Translation;
using Inventory.Services.Web;

namespace Inventory.Services
{
    public static class ServiceManager
    {
        private static DatabaseManager dbManager;
        public static void InitializeServices()
        {
            dbManager = new DatabaseManager();
            SessionService.Initialize();
            TranslationService.Initialize();
            WebService.Initialize();
        }
    }
}
