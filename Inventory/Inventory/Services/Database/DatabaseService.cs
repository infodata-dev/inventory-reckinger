﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Inventory.Services.DependencyServices;

namespace Inventory.Services.Database
{
    public class DatabaseService
    {
        public SQLiteConnection database { set; get; }

        /// <summary>
        /// Initialisation de la base de donnée
        /// </summary>
        /// <param name="tables">Liste des tables à créer</param>
        /// <param name="databaseName">Nom de la base de donnée</param>
        /// <returns></returns>
        public bool Initialize(List<Type> tables, string databaseName)
        {
            try
            {
                database = DependencyService.Get<ISqLiteService>().GetConnexion(databaseName);

                foreach (Type obj in tables)
                {
                    database.CreateTable(obj);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        #region Clear methods

        public bool ClearAllTables(List<string> avoidsTables = null)
        {
            try
            {
                var tableMaps = database.TableMappings;
                foreach (TableMapping tm in tableMaps)
                {
                    if (tm.TableName == "ColumnInfo")
                        continue;

                    if (avoidsTables != null && avoidsTables.Contains(tm.TableName))
                        continue;

                    database.DeleteAll(tm);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool ClearTables(List<string> tables = null)
        {
            try
            {
                var tableMaps = database.TableMappings;
                foreach (TableMapping tm in tableMaps)
                {
                    if (tm.TableName == "ColumnInfo")
                        continue;

                    if (tables == null || !tables.Contains(tm.TableName))
                        continue;

                    database.DeleteAll(tm);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool ClearItems(List<DatabaseModel> items)
        {
            try
            {
                foreach (DatabaseModel item in items)
                {
                    database.Delete(item);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool ClearItem(DatabaseModel item)
        {
            try
            {
                database.Delete(item);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool ClearItem<T>(int itemId) where T : new()
        {
            try
            {
                database.Delete<T>(itemId);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Save methods

        public int SaveItem(DatabaseModel item, bool single = false, int id = -1)
        {
            try
            {
                item.SavingProcess();

                if (single)
                    database.DeleteAll(database.GetMapping(item.GetType()));

                int currentId = item.id;
                if (id != -1)
                {
                    item.id = id;
                    currentId = id;
                }

                if (currentId > 0)
                    return database.Update(item);
                else
                    return database.Insert(item);
            }
            catch
            {
                return -1;
            }
        }
        public bool SaveItems(List<DatabaseModel> items)
        {
            try
            {
                foreach (DatabaseModel item in items)
                {
                    item.SavingProcess();
                    if (item.id != 0)
                    {
                        database.Update(item);
                    }
                    else
                    {
                        database.Insert(item);
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Get methods

        public DatabaseModel GetItem<T>(int id) where T : new()
        {
            try
            {
                T item = database.Get<T>(id);
                ((DatabaseModel)(object)item).ExtractingProcess();
                return ((DatabaseModel)(object)item);
            }
            catch
            {
                return null;
            }
        }
        public DatabaseModel GetFirstItem<T>() where T : new()
        {
            try
            {
                string tableName = database.GetMapping<T>().TableName;
                List<T> items = database.Query<T>($"SELECT * FROM {tableName} ORDER BY id ASC LIMIT 1");
                if (items.Count > 1 || items[0] == null)
                    return null;

                ((DatabaseModel)(object)items[0]).ExtractingProcess();
                return ((DatabaseModel)(object)items[0]);
            }
            catch
            {
                return null;
            }
        }
        public List<T> GetAllItems<T>() where T : new()
        {
            try
            {
                string tableName = database.GetMapping<T>().TableName;
                List<T> items = database.Query<T>($"SELECT * FROM {tableName}");
                foreach(T item in items) {
                    ((DatabaseModel)(object)item).ExtractingProcess();
                }
                return items;
            }
            catch
            {
                return null;
            }
        }
        public List<T> GetAllItemsWithQuery<T>(string query) where T : new()
        {
            try
            {
                List<T> items = database.Query<T>(query);
                foreach (T item in items)
                {
                    ((DatabaseModel)(object)item).ExtractingProcess();
                }
                return items;
            }
            catch
            {
                return null;
            }
        }

        #endregion

        #region Other methods

        public bool HasItems<T>() where T : new()
        {
            return GetAllItems<T>().Count > 0 ? true : false;
        }

        #endregion

    }
}
