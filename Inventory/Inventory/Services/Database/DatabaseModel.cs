﻿using Prism.Mvvm;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Services.Database
{
    public class DatabaseModel : BindableBase
    {
        [PrimaryKey, AutoIncrement]
        public int id { set; get; }

        public void SavingProcess()
        {
            ProcessBeforeSaving();
        }

        public void ExtractingProcess()
        {
            ProcessBeforeExtracting();
        }

        protected virtual void ProcessBeforeSaving() { }
        protected virtual void ProcessBeforeExtracting() { }
    }
}
