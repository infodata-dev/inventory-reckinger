﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Essentials;

namespace Inventory.Services.Database
{
    public class DatabaseManager
    {
        public static DatabaseManager Standalone { set; get; }

        public DatabaseService Services;

        public DatabaseManager()
        {
            Standalone = this;

            Services = new DatabaseService();

            string databaseName = (AppInfo.Name.ToLower() + ".db3").Replace(" ","");
            Services.Initialize(InitializeModels(), databaseName);
        }

        public List<Type> InitializeModels()
        {
            var baseType = typeof(DatabaseModel);
            var assembly = baseType.Assembly;
            return assembly.GetTypes().Where(t => t.BaseType == baseType).ToList();
        }

    }
}
