﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Services.Web
{
    public class WSRequest
    {
        private Func<Object, Task<WSResult>> action { get; set; }
        public Uri uri { get; set; }
        public StringContent content { get; set; }
        public Dictionary<string, string> dataContent { get; set; }

        public WSType type { get; set; }
        public enum WSType
        {
            DISABLE,
            POST,
            GET,
            OPTION,
            URLENCODED
        }

        #region Constructor

        public WSRequest(Func<Object, Task<WSResult>> a, StringContent s, Uri u, WSType t = WSType.DISABLE)
        {
            action = a;
            uri = u;

            if (t == WSType.DISABLE)
            {
                if (s == null)
                {
                    type = WSType.GET;
                }
                else
                {
                    type = WSType.POST;
                    content = s;
                }
            }
            else
            {
                if (s != null)
                    content = s;

                type = t;
            }
        }

        public WSRequest(Func<Object, Task<WSResult>> a, Dictionary<string, string> data, Uri u)
        {
            action = a;
            uri = u;

            if (data == null)
            {
                type = WSType.GET;
            }
            else
            {
                dataContent = data;
                type = WSType.URLENCODED;
            }
        }

        #endregion

        #region Methods
        public Task<WSResult> Close(Object r)
        {
            if (action == null)
                return Task.FromResult<WSResult>(new WSResult(true));

            return action.Invoke(r);
        }
        public async Task<HttpResponseMessage> Connect(HttpClient c)
        {
            switch (type)
            {
                case WSType.POST:
                    return await c.PostAsync(uri, content);

                case WSType.GET:
                    return await c.GetAsync(uri);

                case WSType.OPTION:
                    var request = new HttpRequestMessage(HttpMethod.Options, uri);
                    return await c.SendAsync(request);

                case WSType.URLENCODED:
                    var UrlEncodedRequest = new HttpRequestMessage(HttpMethod.Post, uri) { Content = new FormUrlEncodedContent(dataContent) };
                    return await c.SendAsync(UrlEncodedRequest);

                default:
                    return null;
            }
        }
        #endregion
    }
}
