﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Services.Web
{
    public static class WebService
    {
        /// <summary>
        /// HttpClient actuel
        /// </summary>
        public static HttpClient client { get; set; }

        /// <summary>
        /// Initialize le service
        /// </summary>
        /// <param name="link"></param>
        /// <returns></returns>
        public static void Initialize()
        {
            client = new HttpClient();
        }

        /// <summary>
        /// Utilisation d'une requête Web Services
        /// </summary>
        /// <param name="rqst">Requête à envoyer</param>
        /// <param name="retry">Tentatives de connexion supplémentaires en cas d'échec</param>
        /// <param name="bypass">Code permettant de passer par dessus des erreurs</param>
        /// <param name="act">Action supplémentaire invoquée si la requête se déroule avec succès et utilise la réponse</param>
        /// <returns></returns>
        public static async Task<WSResult> Request(WSRequest rqst, int retry, string bypass = "", Action<HttpResponseMessage> act = null)
        {
            HttpResponseMessage response = null;

            // Permet de reconnaitre une requête dans les logs.
            string idLog = new Random().Next(0, 999999).ToString();

            int r = 0;
            while (response == null && r < retry)
            {
                try
                {
                    response = await rqst.Connect(client);
                }
                catch
                {
                    r += 1;
                }
            }

            if (response == null)
            {
                if (bypass.Contains("NC"))
                {
                    await rqst.Close(response);
                    // LoggingService.oldService.Save(9002, "[ALERT] Web Service : WS (" + idLog + ") on n'analyse pas la réponse");
                    return new WSResult(true, "NC");
                }

                // LoggingService.oldService.Save(9001, "[ERROR] Web Service : WS (" + idLog + ") pas de connexion internet");
                return new WSResult("NO INTERNET CONNECTION, Http response = NULL");
            }

            if (bypass.Contains("ALL"))
            {
                await rqst.Close(response);
                // LoggingService.oldService.Save(9003, "[ALERT] Web Service : WS (" + idLog + ") on n'analyse pas la réponse mais elle n'est pas vide");
                return new WSResult(true, "ALL");
            }

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:

                    var result = "";
                    if (response != null)
                    {
                        result = await ((HttpResponseMessage)response).Content.ReadAsStringAsync();

                        if (act != null)
                            act.Invoke(response);
                    }

                    // LoggingService.oldService.Save(9200, "[SUCCESS] Web Service : WS (" + idLog + ") renvoi 200");
                    return await rqst.Close(result);

                case HttpStatusCode.BadGateway: // 502 : An intermediate proxy server received a bad response from another proxy or the origin server.
                    // LoggingService.oldService.Save(9502, "[ALERT] Web Service : WS (" + idLog + ") renvoi 502 : " + response.ReasonPhrase);
                    return bypass.Contains("502") ? new WSResult(true, "502") : new WSResult("[502] : " + response.ReasonPhrase);

                case HttpStatusCode.GatewayTimeout: // 504 : Indicates that the requested resource is no longer available.
                    // LoggingService.oldService.Save(9504, "[ALERT] Web Service : WS (" + idLog + ") renvoi 504 : " + response.ReasonPhrase);
                    return bypass.Contains("504") ? new WSResult(true, "504") : new WSResult("[504] : " + response.ReasonPhrase);

                case HttpStatusCode.HttpVersionNotSupported: // 505 : Indicates that the requested HTTP version is not supported by the server.
                    // LoggingService.oldService.Save(9505, "[ALERT] Web Service : WS (" + idLog + ") renvoi 505 : " + response.ReasonPhrase);
                    return bypass.Contains("505") ? new WSResult(true, "505") : new WSResult("[505] : " + response.ReasonPhrase);

                case HttpStatusCode.InternalServerError: // 500 : Indicates that a generic error has occurred on the server.
                    // LoggingService.oldService.Save(9500, "[ALERT] Web Service : WS (" + idLog + ") renvoi 500 : " + response.ReasonPhrase);
                    return bypass.Contains("500") ? new WSResult(true, "500") : new WSResult("[500] : " + response.ReasonPhrase);

                case HttpStatusCode.NotImplemented: // 501 : Indicates that the server does not support the requested function.
                    // LoggingService.oldService.Save(9501, "[ALERT] Web Service : WS (" + idLog + ") renvoi 501 : " + response.ReasonPhrase);
                    return bypass.Contains("501") ? new WSResult(true, "501") : new WSResult("[501] : " + response.ReasonPhrase);

                case HttpStatusCode.ServiceUnavailable: // 503 : Indicates that the server is temporarily unavailable, usually due to high load or maintenance.
                    // LoggingService.oldService.Save(9503, "[ALERT] Web Service : WS (" + idLog + ") renvoi 503 : " + response.ReasonPhrase);
                    return bypass.Contains("503") ? new WSResult(true, "503") : new WSResult("[503] : " + response.ReasonPhrase);

                case HttpStatusCode.NotFound: // 404 : Indicates that the requested resource does not exist on the server.
                    // LoggingService.oldService.Save(9404, "[ALERT] Web Service : WS (" + idLog + ") renvoi 404 : " + response.ReasonPhrase);
                    return bypass.Contains("404") ? new WSResult(true, "404") : new WSResult("[404] : " + response.ReasonPhrase);

                case HttpStatusCode.BadRequest: // 400 : BadRequest indicates that the request could not be understood by the server.
                    // LoggingService.oldService.Save(9400, "[ALERT] Web Service : WS (" + idLog + ") renvoi 400 : " + response.ReasonPhrase);
                    return bypass.Contains("400") ? new WSResult(true, "400") : new WSResult("[400] : " + response.ReasonPhrase);

                case HttpStatusCode.Conflict: // 409 :  Indicates that the request could not be carried out because of a conflict on the server.
                    // LoggingService.oldService.Save(9409, "[ALERT] Web Service : WS (" + idLog + ") renvoi 409 : " + response.ReasonPhrase);
                    return bypass.Contains("409") ? new WSResult(true, "409") : new WSResult("[409] : " + response.ReasonPhrase);

                case HttpStatusCode.Forbidden: // 403 : Indicates that the server refuses to fulfill the request.
                    // LoggingService.oldService.Save(9403, "[ALERT] Web Service : WS (" + idLog + ") renvoi 403 : " + response.ReasonPhrase);
                    return bypass.Contains("403") ? new WSResult(true, "403") : new WSResult("[403] : " + response.ReasonPhrase);

                default:
                    // LoggingService.oldService.Save(1013, "[ERROR] Web Service : WS (" + idLog + ") renvoi une erreur inconnue : " + response.ReasonPhrase);
                    return new WSResult(response.ReasonPhrase);
            }
        }
    }
}
