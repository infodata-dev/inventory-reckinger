﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Services.Web
{
    public class WSResult
    {
        private string information { set; get; }
        private bool statut { set; get; }
        private Object parameter { set; get; }

        public WSResult(string info = "")
        {
            statut = false;
            information = info;
            parameter = null;
        }

        public WSResult(bool b, string info = "")
        {
            statut = b;
            information = info;
            parameter = null;
        }

        public WSResult(bool b, Object para, string info = "")
        {
            statut = b;
            information = info;
            parameter = para;
        }

        public string GetInfo()
        {
            return information;
        }

        public bool IsSuccessed()
        {
            return statut;
        }

        public Object GetParameter()
        {
            return parameter;
        }
    }
}
