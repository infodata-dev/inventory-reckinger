﻿using Inventory.Models;
using Inventory.Services.Database;
using Inventory.Services.DependencyServices;
using Inventory.Services.Session;
using Inventory.Services.Web;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Inventory.Services.Article
{
    class ArticleService
    {
        public static async Task<WSResult> getArticleWS()
        {
            //récupération des articles depuis un web service
            string url = null;
            url = SessionService.currentConfig == null ? null : SessionService.currentConfig.Url;
            string pdaId = DependencyService.Get<IDeviceInfoService>().GetIdentifier();
            url = url + "/articles/?PDAID=" + pdaId;
            var uri = new Uri(string.Format(url, string.Empty));

            Func<Object, Task<WSResult>> a = (r) =>
            {
                var result = (string)r;

                if (String.IsNullOrEmpty(result) || result == "[]")
                {
                    return Task.FromResult(new WSResult("Result is empty !"));
                }
                List<ArticleModel> articles = JsonConvert.DeserializeObject<List<ArticleModel>>(result);
                if (cleanDataBaseArticle())
                {
                    foreach (ArticleModel article in articles)
                    {
                        fillDataBaseArticle(article);
                    }
                }
                return Task.FromResult(new WSResult(true));
            };

            WSRequest rqst = new WSRequest(a, null, uri, WSRequest.WSType.GET);
            return await WebService.Request(rqst, 5, "");
        }

        public static bool cleanDataBaseArticle()
        {
            List<string> tabel = new List<string>
            {
                "ArticleModel"
            };
            return DatabaseManager.Standalone.Services.ClearTables(tabel);
        }

        public static void fillDataBaseArticle(ArticleModel article)
        {
            DatabaseManager.Standalone.Services.SaveItem(article);
        }

        public static String checkItemInDatabase(String barCode)
        {
            string query = $"SELECT * FROM ArticleModel WHERE ArticleCode = '" + barCode + "'";
            List<ArticleModel> article = DatabaseManager.Standalone.Services.GetAllItemsWithQuery<ArticleModel>(query);
            if (article.Count == 0)
            {
                return "";
            }
            else
            {
                return article[0].ArticleLabel;
            }
        }

        public static String checkItemInDatabaseInventory(String barCode, String group)
        {
            string query = $"SELECT * FROM ArticleModel WHERE ArticleCode = '" + barCode + "' AND ArticleGroup = '" + group + "'";
            List<ArticleModel> article = DatabaseManager.Standalone.Services.GetAllItemsWithQuery<ArticleModel>(query);
            if (article.Count == 0)
            {
                return "";
            }
            else
            {
                return article[0].ArticleLabel;
            }
        }

    }
}
