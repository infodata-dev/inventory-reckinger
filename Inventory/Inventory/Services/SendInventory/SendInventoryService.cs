﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Inventory.Models;
using Inventory.Services.DependencyServices;
using Inventory.Services.Session;
using Inventory.Services.Web;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Inventory.Services.SendInventory
{
    class SendInventoryService
    {
        public static async Task<WSResult> PostInventory(List<InventoryModel> inventories ,ConfigModel config = null)
        {
            string url = null;
            if (config != null)
            {
                url = config.Url;
            }
            else
            {
                url = SessionService.currentConfig == null ? null : SessionService.currentConfig.Url;
            }

            if (String.IsNullOrEmpty(url)){
                return new WSResult("Can't find a correct URL.");
            }
            string pdaId = DependencyService.Get<IDeviceInfoService>().GetIdentifier();
            url += "/inventories?PDAID=" + pdaId;

            var uri = new Uri(string.Format(url, string.Empty));
            StringContent content = null;

            String stringJsonInventories = "";
            
            stringJsonInventories += "[{\"invDate\":\"" + inventories[0].Date + "\", \"invHeure\":\"" + inventories[0].Hour + "\", \"invType\":\"" + inventories[0].Type + "\", \"invRef\":\"" + inventories[0].Reference + "\", \"invArt\":";
            stringJsonInventories += JsonConvert.SerializeObject(inventories);
            stringJsonInventories += "}]";
            try
            {
                content = new StringContent(stringJsonInventories, Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                return new WSResult("Json content of the sending object is not correct : " + ex.Message);
            }

            if (content == null)
            {
                return new WSResult("Json content of the sending object is not correct.");
            }

            Func<Object, Task<WSResult>> a = (r) =>
            {
                var result = (string)r;

                if (String.IsNullOrEmpty(result) || result == "[]")
                {
                    //return Task.FromResult(new WSResult("Result is empty !"));
                }

                return Task.FromResult(new WSResult(true));
            };

            WSRequest rqst = new WSRequest(a, content, uri, WSRequest.WSType.POST);
            return await WebService.Request(rqst, 5, "");
        }
    }
}
