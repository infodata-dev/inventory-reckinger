﻿using System;
using System.Collections.Generic;
using System.Text;
using Inventory.Resources.Lge;

namespace Inventory.Services.Translation
{
    public static class TranslationService
    {
        private static TranslateExtension translateExtension;

        /// <summary>
        /// Initialise le service
        /// </summary>
        public static void Initialize()
        {
            translateExtension = new TranslateExtension();
        }

        /// <summary>
        /// Fourni la traduction du mot lié à la clée
        /// </summary>
        /// <param name="id">Clé</param>
        /// <returns></returns>
        public static string getTranslatedLabel(string id)
        {
            return translateExtension.ProvideString(id);
        }
    }
}
