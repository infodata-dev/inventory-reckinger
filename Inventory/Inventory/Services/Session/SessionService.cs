﻿using System;
using System.Collections.Generic;
using System.Text;
using Inventory.Models;
using Inventory.Services.Database;

namespace Inventory.Services.Session
{
    public static class SessionService
    {
        // L'utilisateur est il actuellement connecté ?
        public static bool isConnected { get; private set; }

        // Config utilisée pour la connexion
        public static ConfigModel currentConfig { get; private set; }

        /// <summary>
        /// Initialise le service
        /// </summary>
        public static void Initialize()
        {
            isConnected = false;

            // Récupère la config enregistrée ou en crée une si il n'y en a pas
            if(DatabaseManager.Standalone.Services.HasItems<ConfigModel>())
                currentConfig = (ConfigModel)DatabaseManager.Standalone.Services.GetFirstItem<ConfigModel>();
            else {
                currentConfig = new ConfigModel();
                DatabaseManager.Standalone.Services.SaveItem(currentConfig, true);
                currentConfig = (ConfigModel)DatabaseManager.Standalone.Services.GetFirstItem<ConfigModel>();
            }
        }
    }
}
