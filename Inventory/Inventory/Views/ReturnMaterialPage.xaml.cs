﻿using Inventory.Models;
using Inventory.Services.Article;
using Inventory.Services.Database;
using Inventory.ViewModels;
using Inventory.Views.Popup;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inventory.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReturnMaterialPage : ContentPage
    {
        #region Properties

        //initialisation
        public ReturnMaterialPage()
        {
            InitializeComponent();
            InitializeMessages();
            order_number_input.Focus();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await Task.Delay(100);
            await Task.Run(() =>
            {
                if (String.IsNullOrEmpty(order_number_input.Text))
                {
                    order_number_input.Focus();
                }
                else
                {
                    bar_code_input.Focus();
                }
            });
            ArticleLabel.Text = "";
        }

        #endregion

        #region Méthode

        // validation de la commande
        private void order_number_input_Completed(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(order_number_input.Text))
            {
                order_number_input.Focus();
            }
            else if (String.IsNullOrEmpty(bar_code_input.Text))
            {
                bar_code_input.Focus();
            }
            else if (String.IsNullOrEmpty(quantity_input.Text))
            {
                quantity_input.Focus();
            }
            else
            {
                ((ReturnMaterialViewModel)this.BindingContext).SaveInventory.Execute();
                bar_code_input.Text = "";
                quantity_input.Text = "";
                ArticleLabel.Text = "";
                bar_code_input.Focus();
            }
        }

        // validation de la quantité
        private void quantity_input_Completed(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(order_number_input.Text))
            {
                order_number_input.Focus();
            }
            else if (String.IsNullOrEmpty(bar_code_input.Text))
            {
                bar_code_input.Focus();
            }
            else if (String.IsNullOrEmpty(quantity_input.Text))
            {
                quantity_input.Focus();
            }
            else
            {
                ((ReturnMaterialViewModel)this.BindingContext).SaveInventory.Execute();
                bar_code_input.Text = "";
                quantity_input.Text = "";
                ArticleLabel.Text = "";
                bar_code_input.Focus();
            }
        }

        //validation du code barre
        private void bar_code_input_Completed(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(bar_code_input.Text))
            {
                String itemLabel = ArticleService.checkItemInDatabase(bar_code_input.Text);
                if (itemLabel == "")
                {
                    String errorLabel = "L'article " + bar_code_input.Text + " n'existe pas dans la base de données.";
                    PopupNavigation.Instance.PushAsync(new PopupError(errorLabel), true);
                }
                ArticleLabel.Text = itemLabel;
            }
            if (String.IsNullOrEmpty(order_number_input.Text))
            {
                order_number_input.Focus();
            }
            else if(String.IsNullOrEmpty(quantity_input.Text))
            {
                quantity_input.Focus();
            }
            else if (String.IsNullOrEmpty(bar_code_input.Text))
            {
                bar_code_input.Focus();
            }
            else
            {
                ((ReturnMaterialViewModel)this.BindingContext).SaveInventory.Execute();
                bar_code_input.Text = "";
                quantity_input.Text = "";
                ArticleLabel.Text = "";
                bar_code_input.Focus();
            }
        }

        #endregion

        #region Messages
        private void InitializeMessages()
        {
           
        }

        #endregion

        private void Entry_Quantity_Update_Unfocused(object sender, FocusEventArgs e)
        {
            var inventoryUpdate = ((Entry)sender).BindingContext;
            DatabaseManager.Standalone.Services.SaveItem((InventoryModel)inventoryUpdate,false, ((InventoryModel)inventoryUpdate).id);
        }
    }
}