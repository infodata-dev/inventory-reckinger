﻿using Inventory.Services.DependencyServices;
using Inventory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inventory.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ParameterPage : ContentPage
    {
        private bool creating = true;
        public ParameterPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            CheckKiosk();
            creating = false;
        }

        private void CheckKiosk()
        {
            ToggledKiosk.IsToggled = DependencyService.Get<IModeKioskService>().GetSwitchModeKiosk();
        }

        private void ToggledKiosk_Toggled(object sender, ToggledEventArgs e)
        {
            if (!creating)
            {
                ((ParameterPageViewModel)this.BindingContext).SwitchKioskCommand.Execute();
                CheckKiosk();
            }
        }
    }
}