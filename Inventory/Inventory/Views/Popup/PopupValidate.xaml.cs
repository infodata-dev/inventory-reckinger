﻿using Inventory.ViewModels;
using Prism.Commands;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inventory.Views.Popup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopupValidate : PopupPage
    {
        public PopupValidate(Object bindingContext)
        {
            this.BindingContext = bindingContext;
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        protected override bool OnBackButtonPressed()
        {
            return base.OnBackButtonPressed();
        }

        protected override bool OnBackgroundClicked()
        {
            return base.OnBackgroundClicked();
        }

        private async void Button_Released_Yes(object sender, EventArgs e)
        {
            foreach (PropertyInfo propertyInfo in this.BindingContext.GetType().GetProperties())
            {
                if (propertyInfo.Name == "SendInventory")
                {
                    try
                    {
                        DelegateCommand command = propertyInfo.GetValue(this.BindingContext) as DelegateCommand;
                        command.Execute();
                    }
                    catch { };
                }
            }
            await PopupNavigation.Instance.PopAsync();
        }

        private async void Button_Released_No(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }
    }
}