﻿using Inventory.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventory.Models;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Rg.Plugins.Popup.Services;
using System.Reflection;
using Prism.Commands;

namespace Inventory.Views.Popup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopupDelete : PopupPage
    {
        private InventoryModel Inventory;
        public PopupDelete(Object bindingContext, InventoryModel inventory)
        {
            this.BindingContext = bindingContext;
            Inventory = inventory;
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            LabelBarCode.Text = Inventory.ScanCode;
            LabelQuantity.Text = Inventory.Quantity.ToString();
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        protected override bool OnBackButtonPressed()
        {
            return base.OnBackButtonPressed();
        }

        protected override bool OnBackgroundClicked()
        {
            return base.OnBackgroundClicked();
        }

        private async void Button_Released_Yes(object sender, EventArgs e)
        {
            foreach(PropertyInfo propertyInfo in this.BindingContext.GetType().GetProperties())
            {
                if(propertyInfo.Name == "DeleteInventory")
                {
                    try
                    {
                        DelegateCommand<InventoryModel> command = propertyInfo.GetValue(this.BindingContext) as DelegateCommand<InventoryModel>;
                        command.Execute(Inventory);
                    }
                    catch { };
                }
            }
            await PopupNavigation.Instance.PopAsync();
        }

        private async void Button_Released_No(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }
    }
}