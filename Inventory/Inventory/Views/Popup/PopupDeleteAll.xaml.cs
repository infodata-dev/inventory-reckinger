﻿using Inventory.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventory.Models;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Rg.Plugins.Popup.Services;
using System.Reflection;
using Prism.Commands;

namespace Inventory.Views.Popup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopupDeleteAll : PopupPage
    {
        private InventoryModel Inventory;
        public PopupDeleteAll(Object bindingContext)
        {
            this.BindingContext = bindingContext;
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        protected override bool OnBackButtonPressed()
        {
            return base.OnBackButtonPressed();
        }

        protected override bool OnBackgroundClicked()
        {
            return base.OnBackgroundClicked();
        }

        private async void Button_Released_Yes(object sender, EventArgs e)
        {
            foreach(PropertyInfo propertyInfo in this.BindingContext.GetType().GetProperties())
            {
                if(propertyInfo.Name == "DeleteAllInventory")
                {
                    try
                    {
                        DelegateCommand command = propertyInfo.GetValue(this.BindingContext) as DelegateCommand;
                        command.Execute();
                    }
                    catch { };
                }
            }
            await PopupNavigation.Instance.PopAsync();
        }

        private async void Button_Released_No(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }
    }
}