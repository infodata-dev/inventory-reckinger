﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inventory.Views.Popup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopupLoading : PopupPage
    {
        private Action closeAction;
        public PopupLoading(Object bindingContext, Action act = null)
        {
            this.BindingContext = bindingContext;
            closeAction = act;
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            if (closeAction != null)
                closeAction.Invoke();
            base.OnDisappearing();
        }

        protected override bool OnBackButtonPressed()
        {
            //return base.OnBackButtonPressed();
            return false;
        }

        protected override bool OnBackgroundClicked()
        {
            //return base.OnBackgroundClicked();
            return false;
        }

        private async void Button_Released_Close(object sender, EventArgs e)
        {
        }
    }
}