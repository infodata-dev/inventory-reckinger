using Inventory.Services;
using Inventory.Services.UI;
using Inventory.ViewModels;
using Inventory.Views;
using Prism;
using Prism.Ioc;
using Xamarin.Essentials.Implementation;
using Xamarin.Essentials.Interfaces;
using Xamarin.Forms;

namespace Inventory
{
    public partial class App
    {
        public App(IPlatformInitializer initializer)
            : base(initializer)
        {
        }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            ServiceManager.InitializeServices();

            UIService.SetTheme("Dark");

            await NavigationService.NavigateAsync("NavigationPage/HomePage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IAppInfo, AppInfoImplementation>();

            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<InventoryPage, InventoryViewModel>("InventoryPage");
            containerRegistry.RegisterForNavigation<ReplenishmentPage, ReplenishmentViewModel>("ReplenishmentPage");
            containerRegistry.RegisterForNavigation<ReturnMaterialPage, ReturnMaterialViewModel>("ReturnMaterialPage");
            containerRegistry.RegisterForNavigation<OutputMaterialPage, OutputMaterialViewModel>("OutputMaterialPage");
            containerRegistry.RegisterForNavigation<ParameterPage, ParameterPageViewModel>("ParameterPage");
            containerRegistry.RegisterForNavigation<HomePage, HomePageViewModel>("HomePage");
            containerRegistry.RegisterForNavigation<OrderPreparationPage, OrderPreparationViewModel>("OrderPreparationPage");
        }
    }
}
