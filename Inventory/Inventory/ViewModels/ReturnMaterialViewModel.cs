﻿using Inventory.Models;
using Inventory.Services.Database;
using Inventory.Services.SendInventory;
using Inventory.Services.Web;
using Inventory.Views.Popup;
using Prism.Commands;
using Prism.Navigation;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Inventory.ViewModels
{
    public class OutputMaterialViewModel : ViewModelBase
    {
        #region Properties
        private bool _isEnable;
        public bool IsEnable
        {
            get { return _isEnable; }
            set { SetProperty(ref _isEnable, value); }
        }

        private string _barCode;
        public string BarCode
        {
            get { return _barCode; }
            set { SetProperty(ref _barCode, value); }
        }

        private string _orderNumber;
        public string OrderNumber
        {
            get { return _orderNumber; }
            set { SetProperty(ref _orderNumber, value); }
        }

        private bool _orderNumberDisable;
        public bool OrderNumberDisable
        {
            get { return _orderNumberDisable; }
            set { SetProperty(ref _orderNumberDisable, value); }
        }

        private string _quantity;
        public string Quantity
        {
            get { return _quantity; }
            set { SetProperty(ref _quantity, value); }
        }

        private ObservableCollection<InventoryModel> _inventoryList;
        public ObservableCollection<InventoryModel> InventoryList 
        { 
            get { return _inventoryList; }
            set { SetProperty(ref _inventoryList, value); }
        }

        private string _entryPwd;
        public string EntryPwd
        {
            get { return _entryPwd; }
            set { SetProperty(ref _entryPwd, value); }
        }

        private bool _isHidden;
        public bool IsHidden
        {
            get { return _isHidden; }
            set { SetProperty(ref _isHidden, value); }
        }

        private String errorLabel = "";
        #endregion

        //initialisation
        public OutputMaterialViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            initializeProperties();
            initializeCommands();
            initializeMessages();
            RefreshListViewInventory();
            checkInventorySend();
        }

        public void initializeProperties()
        {

        }

        #region Méthodes
        // rafraichir la list de l'inventaire
        public void RefreshListViewInventory()
        {
            OrderNumberDisable = false;
            string query = $"SELECT * FROM InventoryModel WHERE Status = 0 AND Type = 'OUTSTOCK' ORDER BY id DESC";
            List<InventoryModel> inventories = DatabaseManager.Standalone.Services.GetAllItemsWithQuery<InventoryModel>(query);
            // désactivation de la saisi de la commande si on a des encodages qui ne sont pas envoyer
            if (inventories.Count > 0)
            {
                OrderNumberDisable = true;
                OrderNumber = inventories[0].Reference;
            }
            InventoryList = new ObservableCollection<InventoryModel>(inventories);
        }

        public async void SendInventoryWs(List<InventoryModel> inventories)
        {
            WSResult result = await SendInventoryService.PostInventory(inventories);
            if (result.IsSuccessed())
            {
                foreach (InventoryModel inventory in inventories)
                {
                    inventory.Status = 1;
                    DatabaseManager.Standalone.Services.SaveItem(inventory, false, inventory.id);
                }
            }
            else
            {
                foreach (InventoryModel sendInventory in inventories)
                {
                    sendInventory.Status = 2;
                    DatabaseManager.Standalone.Services.SaveItem(sendInventory, false, sendInventory.id);
                }
                errorLabel = result.GetInfo();
            }
            checkInventorySend();
        }

        public void checkInventorySend()
        {
            String query = $"SELECT * FROM InventoryModel WHERE Status = 2 ORDER BY id DESC";
            List<InventoryModel> inventories = DatabaseManager.Standalone.Services.GetAllItemsWithQuery<InventoryModel>(query);
            if (inventories.Count > 0)
            {
                IsHidden = true;
            }
            else
            {
                IsHidden = false;
            }
        }

        #endregion

        #region Commands

        public DelegateCommand SaveInventory { private set; get; }
        public DelegateCommand SendInventory { private set; get; }
        public DelegateCommand<InventoryModel> DeleteInventory { private set; get; }
        public DelegateCommand DeleteAllInventory { private set; get; }
        public DelegateCommand<InventoryModel> DeletePopup { private set; get; }
        public DelegateCommand DeleteAllPopup { private set; get; }
        public DelegateCommand ValidatePopup { private set; get; }
        public DelegateCommand<string> ChangePageCommand { private set; get; }
        public DelegateCommand PopupPwdCommand { private set; get; }
        public DelegateCommand ReSendInventory { private set; get; }
        public void initializeCommands()
        {
            IsEnable = true;

            //Commande d'enregistement d'un scan et de la quantité
            SaveInventory = new DelegateCommand(
                () =>
                {
                    if (!IsEnable)
                    {
                        return;
                    }
                    IsEnable = false;
                    InventoryModel inventory = new InventoryModel();
                    inventory.ScanCode = BarCode;
                    string query = $"SELECT * FROM ArticleModel WHERE ArticleCode = '" + BarCode + "'";
                    List<ArticleModel> article = DatabaseManager.Standalone.Services.GetAllItemsWithQuery<ArticleModel>(query);
                    if (article.Count > 0)
                    {
                        inventory.LabelItem = article[0].ArticleLabel;
                    }
                    else
                    {
                        inventory.LabelItem = "";
                    }
                    inventory.Quantity = Convert.ToInt32(Quantity);
                    inventory.Status = 0;
                    inventory.Reference = OrderNumber;
                    inventory.Type = "OUTSTOCK";
                    DatabaseManager.Standalone.Services.SaveItem(inventory);
                    RefreshListViewInventory();
                    IsEnable = true;
                },
                () =>
                {
                    return IsEnable;
                }).ObservesProperty(() => IsEnable);
            // Commande d'envoi de l'inventaire au serveur
            SendInventory = new DelegateCommand(
                async () =>
                {
                    if (!IsEnable)
                    {
                        return;
                    }
                    IsEnable = false;
                    errorLabel = "";
                    PopupLoading loading = new PopupLoading(this);
                    await PopupNavigation.Instance.PushAsync(loading, true);
                    DateTime now = DateTime.Now;
                    string query = $"SELECT * FROM InventoryModel WHERE Status = 0 AND Type = 'OUTSTOCK' ORDER BY id DESC";
                    List<InventoryModel> inventories = DatabaseManager.Standalone.Services.GetAllItemsWithQuery<InventoryModel>(query);
                    foreach (InventoryModel inventory in inventories)
                    {
                        inventory.Date = now.ToString("dd/MM/yyyy");
                        inventory.Hour = now.ToString("HH:mm:ss");
                        inventory.Status = 2;
                        DatabaseManager.Standalone.Services.SaveItem(inventory, false, inventory.id);
                    }
                    RefreshListViewInventory();
                    query = $"SELECT * FROM InventoryModel WHERE Status = 2 ORDER BY id DESC";
                    inventories = DatabaseManager.Standalone.Services.GetAllItemsWithQuery<InventoryModel>(query);

                    if (inventories != null && inventories.Any())
                    {
                        List<InventoryModel> invArt = new List<InventoryModel>();
                        string lastDate = "";
                        string lastHour = "";
                        foreach (InventoryModel inventory in inventories)
                        {
                            if (lastDate == "" || lastHour == "" || (lastDate == inventory.Date && lastHour == inventory.Hour))
                            {
                                invArt.Add(inventory);
                                lastDate = inventory.Date;
                                lastHour = inventory.Hour;
                            }
                            else
                            {
                                SendInventoryWs(invArt);
                                invArt = new List<InventoryModel>();
                                invArt.Add(inventory);
                                lastDate = inventory.Date;
                                lastHour = inventory.Hour;
                            }
                        }
                        SendInventoryWs(invArt);
                        checkInventorySend();
                    }
                    OrderNumber = "";
                    await PopupNavigation.Instance.PopAsync();
                    await PopupNavigation.Instance.PopAsync();
                    if (errorLabel != "")
                    {
                        await PopupNavigation.Instance.PushAsync(new PopupError(errorLabel), true);
                    }
                    IsEnable = true;
                },
                () =>
                {
                    return IsEnable;
                }).ObservesProperty(() => IsEnable);
            // suppression d'un inventaire
            DeleteInventory = new DelegateCommand<InventoryModel>(
                (inventory) =>
                {
                    if (!IsEnable)
                    {
                        return;
                    }
                    IsEnable = false;
                    bool test = DatabaseManager.Standalone.Services.ClearItem<InventoryModel>(inventory.id);
                    RefreshListViewInventory();
                    IsEnable = true;
                },
                (inventory) =>
                {
                    return IsEnable;
                }).ObservesProperty(() => IsEnable);

            // suppression de l'inventaire courant
            DeleteAllInventory = new DelegateCommand(
                () =>
                {
                    if (!IsEnable)
                    {
                        return;
                    }
                    IsEnable = false;
                    string query = $"SELECT * FROM InventoryModel WHERE Status = 0 AND Type = 'OUTSTOCK' ORDER BY id DESC";
                    List<InventoryModel> inventories = DatabaseManager.Standalone.Services.GetAllItemsWithQuery<InventoryModel>(query);
                    foreach (InventoryModel inventory in inventories)
                    {
                        bool test = DatabaseManager.Standalone.Services.ClearItem<InventoryModel>(inventory.id);
                    }
                        RefreshListViewInventory();
                    IsEnable = true;
                },
                () =>
                {
                    return IsEnable;
                }).ObservesProperty(() => IsEnable);

            // ouverture popup de validation
            ValidatePopup = new DelegateCommand(
                async () =>
                {
                    if (!IsEnable)
                    {
                        return;
                    }
                    IsEnable = false;
                    await PopupNavigation.Instance.PushAsync(new PopupValidate(this), true);
                    IsEnable = true;
                },
                () =>
                {
                    return IsEnable;
                }).ObservesProperty(() => IsEnable);

            // ouverture popup de supression inventaire
            DeleteAllPopup = new DelegateCommand(
                async () =>
                {
                    if (!IsEnable)
                    {
                        return;
                    }
                    IsEnable = false;
                    await PopupNavigation.Instance.PushAsync(new PopupDeleteAll(this), true);
                    IsEnable = true;
                },
                () =>
                {
                    return IsEnable;
                }).ObservesProperty(() => IsEnable);

            // ouverture popup de suppression
            DeletePopup = new DelegateCommand<InventoryModel>(
                async (inventory) =>
                {
                    if (!IsEnable)
                    {
                        return;
                    }
                    IsEnable = false;
                    await PopupNavigation.Instance.PushAsync(new PopupDelete(this, inventory), true);
                    IsEnable = true;
                },
                (inventory) =>
                {
                    return IsEnable;
                }).ObservesProperty(() => IsEnable);

            //changement de page
            ChangePageCommand = new DelegateCommand<string>(
                async (page) =>
                {
                    if (!IsEnable)
                    {
                        return;
                    }
                    IsEnable = false;
                    ConfigModel pwd = new ConfigModel();
                    if (pwd.paraPwd == EntryPwd || pwd.paraPwd2 == EntryPwd)
                    {
                        await NavigationService.NavigateAsync(page);
                        await PopupNavigation.Instance.PopAsync();
                    }
                    else
                    {
                        string message = "Mot de passe incorrect.";
                        await PopupNavigation.Instance.PushAsync(new PopupError(message), true);
                    }
                    IsEnable = true;
                },
                (page) =>
                {
                    return IsEnable;
                }).ObservesProperty(() => IsEnable);

            //popup pour entrer le mot de passe
            PopupPwdCommand = new DelegateCommand(
                async () =>
                {
                    if (!IsEnable)
                    {
                        return;
                    }
                    IsEnable = false;
                    await PopupNavigation.Instance.PushAsync(new PopupPwd(this), true);
                    IsEnable = true;
                },
                () =>
                {
                    return IsEnable;
                }).ObservesProperty(() => IsEnable);

            // Commande de re-envoi de l'inventaire au serveur
            ReSendInventory = new DelegateCommand(
                async () =>
                {
                    if (!IsEnable)
                    {
                        return;
                    }
                    IsEnable = false;
                    errorLabel = "";
                    PopupLoading loading = new PopupLoading(this);
                    await PopupNavigation.Instance.PushAsync(loading, true);
                    String query = $"SELECT * FROM InventoryModel WHERE Status = 2 ORDER BY id DESC";
                    List<InventoryModel> inventories = DatabaseManager.Standalone.Services.GetAllItemsWithQuery<InventoryModel>(query);

                    if (inventories != null && inventories.Any())
                    {
                        List<InventoryModel> invArt = new List<InventoryModel>();
                        string lastDate = "";
                        string lastHour = "";
                        foreach (InventoryModel inventory in inventories)
                        {
                            if (lastDate == "" || lastHour == "" || (lastDate == inventory.Date && lastHour == inventory.Hour))
                            {
                                invArt.Add(inventory);
                                lastDate = inventory.Date;
                                lastHour = inventory.Hour;
                            }
                            else
                            {
                                SendInventoryWs(invArt);
                                invArt = new List<InventoryModel>();
                                invArt.Add(inventory);
                                lastDate = inventory.Date;
                                lastHour = inventory.Hour;
                            }
                        }
                        SendInventoryWs(invArt);
                        checkInventorySend();
                    }
                    await PopupNavigation.Instance.PopAsync();
                    if (errorLabel != "")
                    {
                        await PopupNavigation.Instance.PushAsync(new PopupError(errorLabel), true);
                    }
                    IsEnable = true;
                },
                () =>
                {
                    return IsEnable;
                }).ObservesProperty(() => IsEnable);
        }
        #endregion

        #region Message
        public void initializeMessages()
        {

        }
        #endregion
    }
}

