﻿using Inventory.Models;
using Inventory.Services.Database;
using Inventory.Services.DependencyServices;
using Inventory.Services.SendInventory;
using Inventory.Services.Web;
using Inventory.Views.Popup;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Xamarin.Forms;

namespace Inventory.ViewModels
{
    public class HomePageViewModel : ViewModelBase
    {
        private bool _isEnable;
        public bool IsEnable
        {
            get { return _isEnable; }
            set { SetProperty(ref _isEnable, value); }
        }

        private string _entryPwd;
        public string EntryPwd
        {
            get { return _entryPwd; }
            set { SetProperty(ref _entryPwd, value); }
        }

        private string _pageName;
        public string PageName
        {
            get { return _pageName; }
            set { SetProperty(ref _pageName, value); }
        }

        private bool _isReadOnly;
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
            set { SetProperty(ref _isReadOnly, value); }
        }
        private bool _isHidden;
        public bool IsHidden
        {
            get { return _isHidden; }
            set { SetProperty(ref _isHidden, value); }
        }


        public HomePageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            initializeProperties();
            initializeCommands();
            initializeMessages();
            checkArticleDatabase();
            checkInventorySend();
        }


        public void initializeProperties()
        {

        }

        public async void SendInventoryWs(List<InventoryModel> inventories)
        {
            WSResult result = await SendInventoryService.PostInventory(inventories);
            if (result.IsSuccessed())
            {
                foreach (InventoryModel inventory in inventories)
                {
                    inventory.Status = 1;
                    DatabaseManager.Standalone.Services.SaveItem(inventory, false, inventory.id);
                }
            }
            else
            {
                foreach (InventoryModel sendInventory in inventories)
                {
                    sendInventory.Status = 2;
                    DatabaseManager.Standalone.Services.SaveItem(sendInventory, false, sendInventory.id);
                }
                await PopupNavigation.Instance.PushAsync(new PopupError(result.GetInfo()), true);
            }
            checkInventorySend();
        }

        public void checkArticleDatabase()
        {
            string query = $"SELECT * FROM ArticleModel";
            List<ArticleModel> article = DatabaseManager.Standalone.Services.GetAllItemsWithQuery<ArticleModel>(query);
            if(article.Count > 0)
            {
                IsReadOnly = true;
            }
            else
            {
                IsReadOnly = false;
            }
        }

        public void checkInventorySend()
        {
            String query = $"SELECT * FROM InventoryModel WHERE Status = 2 ORDER BY id DESC";
            List<InventoryModel> inventories = DatabaseManager.Standalone.Services.GetAllItemsWithQuery<InventoryModel>(query);
            if(inventories.Count > 0)
            {
                IsHidden = true;
            }
            else
            {
                IsHidden = false;
            }
        }



        #region Commands

        public DelegateCommand<string> ChangePageCommand { private set; get; }
        public DelegateCommand PopupPwdCommand { private set; get; }
        public DelegateCommand SynchronisationCommand { private set; get; }
        public DelegateCommand ReSendInventory { private set; get; }
        public DelegateCommand DownloadApk { private set; get; }


        public void initializeCommands()
        {
            IsEnable = true;
            //changement de page
            ChangePageCommand = new DelegateCommand<string>(
                async (page) =>
                {
                    if (!IsEnable)
                    {
                        return;
                    }
                    IsEnable = false;
                    if (page == "ParameterPage")
                    {
                        ConfigModel pwd = new ConfigModel();
                        if (pwd.paraPwd == EntryPwd || pwd.paraPwd2 == EntryPwd)
                        {
                            await NavigationService.NavigateAsync(page);
                            await PopupNavigation.Instance.PopAsync();
                        }
                        else
                        {
                            string message = "Mot de passe incorrect.";
                            await PopupNavigation.Instance.PushAsync(new PopupError(message), true);
                        }
                    }
                    else
                    {
                        await NavigationService.NavigateAsync(page);
                    }
                    
                    IsEnable = true;
                },
                (page) =>
                {
                    return IsEnable;
                }).ObservesProperty(() => IsEnable);

            //popup pour entrer le mot de passe
            PopupPwdCommand = new DelegateCommand(
                async () =>
                {
                    if (!IsEnable)
                    {
                        return;
                    }
                    IsEnable = false;
                    await PopupNavigation.Instance.PushAsync(new PopupPwd(this, () => { checkArticleDatabase(); }), true);
                    IsEnable = true;
                },
                () =>
                {
                    return IsEnable;
                }).ObservesProperty(() => IsEnable);

            //commande de synchronisation article voir envoi inventaire
            SynchronisationCommand = new DelegateCommand(
                async () =>
                {
                    if (!IsEnable)
                    {
                        return;
                    }
                    IsEnable = false;
                    PopupLoading loading = new PopupLoading(this, () => { checkArticleDatabase(); });
                    await PopupNavigation.Instance.PushAsync(loading, true);
                    await Services.Article.ArticleService.getArticleWS();
                    await PopupNavigation.Instance.PopAsync();
                    checkArticleDatabase();
                    IsEnable = true;
                },
                () =>
                {
                    return IsEnable;
                }).ObservesProperty(() => IsEnable);

            // download APK
            DownloadApk = new DelegateCommand(
                async() =>
                {
                    IsEnable = false;
                    PopupLoading loading = new PopupLoading(this, () => { checkArticleDatabase(); });
                    await PopupNavigation.Instance.PushAsync(loading, true);
                    CancellationToken token = new CancellationToken();
                    await DependencyService.Get<IUpdateService>().DownloadAndUpdateApp(token, "https://www.always-on.lu/Android/Inventory.apk");
                    await PopupNavigation.Instance.PopAsync();
                    checkArticleDatabase();
                    IsEnable = true;
                },
                () =>
                {
                    return IsEnable;
                }).ObservesProperty(() => IsEnable);
            // Commande de re-envoi de l'inventaire au serveur
            ReSendInventory = new DelegateCommand(
                async () =>
                {
                    if (!IsEnable)
                    {
                        return;
                    }
                    IsEnable = false;
                    PopupLoading loading = new PopupLoading(this, () => { checkArticleDatabase(); });
                    await PopupNavigation.Instance.PushAsync(loading, true);
                    String query = $"SELECT * FROM InventoryModel WHERE Status = 2 ORDER BY id DESC";
                    List<InventoryModel> inventories = DatabaseManager.Standalone.Services.GetAllItemsWithQuery<InventoryModel>(query);

                    if (inventories != null && inventories.Any())
                    {
                        List<InventoryModel> invArt = new List<InventoryModel>();
                        string lastDate = "";
                        string lastHour = "";
                        foreach (InventoryModel inventory in inventories)
                        {
                            if (lastDate == "" || lastHour == "" || (lastDate == inventory.Date && lastHour == inventory.Hour))
                            {
                                invArt.Add(inventory);
                                lastDate = inventory.Date;
                                lastHour = inventory.Hour;
                            }
                            else
                            {
                                SendInventoryWs(invArt);
                                invArt = new List<InventoryModel>();
                                invArt.Add(inventory);
                                lastDate = inventory.Date;
                                lastHour = inventory.Hour;
                            }
                        }
                        SendInventoryWs(invArt);
                        checkInventorySend();
                        checkArticleDatabase();
                    }
                    await PopupNavigation.Instance.PopAsync();
                    IsEnable = true;
                },
                () =>
                {
                    return IsEnable;
                }).ObservesProperty(() => IsEnable);
        }
        #endregion

        #region Message
        public void initializeMessages()
        {

        }
        #endregion

        #region Navigation

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
        }

        #endregion
    }
}
