﻿using Inventory.Models;
using Inventory.Services.Database;
using Inventory.Services.DependencyServices;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace Inventory.ViewModels
{
    public class ParameterPageViewModel : ViewModelBase
    {
        private bool _isEnabled;
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { SetProperty(ref _isEnabled, value); }
        }

        private string _url;
        public string Url
        {
            get { return _url; }
            set { SetProperty(ref _url, value); }
        }

        private string _imei;
        public string Imei
        {
            get { return _imei; }
            set { SetProperty(ref _imei, value); }
        }

        public ParameterPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            initializeProperties();
            initializeCommands();
        }
        public void initializeProperties()
        {
            ConfigModel parameter = (ConfigModel)DatabaseManager.Standalone.Services.GetFirstItem<ConfigModel>();
            Url = parameter.Url;
            Imei = DependencyService.Get<IDeviceInfoService>().GetIdentifier();
        }

        public DelegateCommand<string> ChangePageCommand { private set; get; }
        public DelegateCommand ValidateParameterCommand { private set; get; }
        public DelegateCommand SwitchKioskCommand { private set; get; }
        public void initializeCommands()
        {
            IsEnabled = true;
            ChangePageCommand = new DelegateCommand<string>(
                (page) =>
                {
                    if (!IsEnabled)
                    {
                        return;
                    }
                    IsEnabled = false;
                    NavigationService.NavigateAsync(page);
                    IsEnabled = true;
                },
                (page) =>
                {
                    return IsEnabled;
                }).ObservesProperty(() => IsEnabled);

            ValidateParameterCommand = new DelegateCommand(
                () =>
                {
                    if (!IsEnabled)
                    {
                        return;
                    }
                    IsEnabled = false;
                    ConfigModel parameter = (ConfigModel)DatabaseManager.Standalone.Services.GetFirstItem<ConfigModel>();
                    parameter.Url = Url;
                    DatabaseManager.Standalone.Services.SaveItem(parameter, false, parameter.id);
                    Application.Current.MainPage.Navigation.PopAsync();
                    IsEnabled = true;
                },
                () =>
                {
                    return IsEnabled;
                }).ObservesProperty(() => IsEnabled);

            SwitchKioskCommand = new DelegateCommand(
                () =>
                {
                    if (!IsEnabled)
                    {
                        return;
                    }
                    IsEnabled = false;
                    DependencyService.Get<IModeKioskService>().SwitchModeKiosk();
                    IsEnabled = true;
                },
                () =>
                {
                    return IsEnabled;
                }).ObservesProperty(() => IsEnabled);
        }

    }
}
