﻿using Inventory.Services.Database;
using Inventory.Utils;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Models
{
    [Table(nameof(ConfigModel))]
    public class ConfigModel : DatabaseModel
    {
        #region Properties
        //Url
        public string Url { get; set; }

        // Mot de passe pour crypter
        // TO DO => L'enregistrer quelque part
        private const string encryptPwd = "infodd1648";

        public string paraPwd = "AccAdminKios";
        public string paraPwd2 = "Infodd1648";

        #endregion

        public ConfigModel()
        {
            this.Url = "http://mobile.infodata.lu/api/reckinger";
        }

        #region Methods

        protected override void ProcessBeforeSaving()
        {
            // Cryptage du token avant enregistrement dans la DB
            this.Url = EncryptUtils.Encrypt(this.Url, encryptPwd);
        }

        protected override void ProcessBeforeExtracting()
        {
            // Décryptage du token lors de la récupération de l'item dans la DB
            this.Url = EncryptUtils.Decrypt(this.Url, encryptPwd);
        }

        #endregion
    }
}
