﻿using Inventory.Services.Database;
using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Models
{
    [Table(nameof(InventoryModel))]
    public class InventoryModel : DatabaseModel
    {
        #region Properties
        // code barre
        [JsonProperty("id")]
        public string ScanCode { get; set; }

        // label article
        [JsonProperty("label")]
        public string LabelItem { get; set; }

        // quantité
        [JsonProperty("qte")]
        public int Quantity { get; set; }

        //type  INVEN/REAPP/INSTOCK/OUTSTOCK
        [JsonProperty("invType")]
        public String Type { get; set; }

        //référence chantier/client ou rien
        [JsonProperty("invRef")]
        public String Reference { get; set; }

        // envoi au serveur
        [JsonProperty("status")]
        public int Status { get; set; }

        //date de l'inventaire
        [JsonProperty("artDate")]
        public string Date { get; set; }

        //heure de l'inventaire
        [JsonProperty("artHeure")]
        public string Hour { get; set; }
        #endregion
    }
}
