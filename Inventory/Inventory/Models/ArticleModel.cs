﻿using Inventory.Services.Database;
using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Models
{
    [Table(nameof(ArticleModel))]
    public class ArticleModel : DatabaseModel
    {
        #region Properties
        // code article
        [JsonProperty("artId")]
        public string ArticleCode { get; set; }

        // libellé article
        [JsonProperty("artLib")]
        public string ArticleLabel { get; set; }

        //groupe article
        [JsonProperty("artGrp")]
        public int ArticleGroup { get; set; }
        #endregion
    }
}
