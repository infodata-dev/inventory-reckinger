﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Utils
{
    public static class DatabaseUtils
    {
        public static string ListToString(List<string> items, char separator = '²')
        {
            string result = "";
            for (int i = 0; i < items.Count; i++)
            {
                result += items[i];
                if (i < items.Count - 1)
                    result += separator;
            }
            return result;
        }

        public static List<string> StringToList(string item, char separator = '²')
        {
            List<string> result = new List<string>();
            string[] items = item.Split(separator);

            for (int i = 0; i < items.Length; i++)
            {
                if (String.IsNullOrEmpty(items[i]))
                    continue;

                result.Add(items[i]);
            }
            return result;
        }
    }
}
