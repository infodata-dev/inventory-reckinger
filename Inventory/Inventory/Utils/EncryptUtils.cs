﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Inventory.Utils
{
    public static class EncryptUtils
    {
        /// <summary>
        /// Génère un HASH 256 avec une chaine de charactère
        /// </summary>
        /// <param name="input">Chaine de charactère utilisé pour générer le HASH 256</param>
        /// <returns>HASH 256</returns>
        private static string Sha256FromString(string input)
        {
            try
            {
                var message = System.Text.Encoding.UTF8.GetBytes(input);
                SHA256Managed hashString = new SHA256Managed();
        
                string hex = "";
                var hashValue = hashString.ComputeHash(message);
                foreach (byte x in hashValue)
                    hex += String.Format("{0:x2}", x);
        
                return hex;
            }
            catch
            {
                return input;
            }
        }
        
        /// <summary>
        /// Crypte une chaine de charactère avec un mot de passe
        /// </summary>
        /// <param name="input">String à crypter</param>
        /// <param name="pwd">Mot de passe</param>
        /// <returns>String crypté</returns>
        public static string Encrypt(string input, string pwd)
        {
            string hash = Sha256FromString(pwd);
            byte[] data = UTF8Encoding.UTF8.GetBytes(input);
            using(MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
                using(TripleDESCryptoServiceProvider trip = new TripleDESCryptoServiceProvider() { Key=key, Mode=CipherMode.ECB, Padding = PaddingMode.PKCS7 })
                {
                    ICryptoTransform tr = trip.CreateEncryptor();
                    byte[] results = tr.TransformFinalBlock(data, 0, data.Length);
                    return Convert.ToBase64String(results, 0, results.Length);
                }
            }
        }

        /// <summary>
        /// Décrypte une chaine de charactère  avec un mot de passe
        /// </summary>
        /// <param name="input">String à décrypter</param>
        /// <param name="pwd">Mot de passe</param>
        /// <returns>String décrypté</returns>
        public static string Decrypt(string input, string pwd)
        {
            string hash = Sha256FromString(pwd);
            byte[] data = Convert.FromBase64String(input);
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
                using (TripleDESCryptoServiceProvider trip = new TripleDESCryptoServiceProvider() { Key = key, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
                {
                    ICryptoTransform tr = trip.CreateDecryptor();
                    byte[] results = tr.TransformFinalBlock(data, 0, data.Length);
                    return UTF8Encoding.UTF8.GetString(results);
                }
            }
        }
    }
}
