﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inventory.Resources.Lge
{
    [ContentProperty("Text")]
    public class TranslateExtension : IMarkupExtension
    {
        const string ResourceId = "Inventory.Resources.Lge.AppResources";
        static readonly Lazy<ResourceManager> resmgr = new Lazy<ResourceManager>(() => new ResourceManager(ResourceId, typeof(TranslateExtension).GetTypeInfo().Assembly));

        public string Text { get; set; }
        /// <summary>
        /// Fournir le libellé dans la langue du téléphone pour le XAML
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Text == null)
                return "";

            var translation = resmgr.Value.GetString(Text, CultureInfo.CurrentUICulture);
            if (translation == null)
            {
#if DEBUG
                throw new ArgumentException(
                    String.Format("Key '{0}' was not found in resources '{1}' for culture '{2}'.", Text, ResourceId, CultureInfo.CurrentUICulture.TwoLetterISOLanguageName),
                    "Text");
#else
				translation = Text;
#endif
            }
            return translation;
        }

        /// <summary>
        /// Retourne un string avec le libellé dans la langue du téléphone
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public string ProvideString(string id)
        {
            if (id == null)
                return "";

            var translation = resmgr.Value.GetString(id, CultureInfo.CurrentUICulture);

            if (translation == null)
            {
#if DEBUG
                throw new ArgumentException(
                    String.Format("Key '{0}' was not found in resources '{1}' for culture '{2}'.", Text, ResourceId, CultureInfo.CurrentUICulture.ThreeLetterISOLanguageName),
                    "Text");
#else
				translation = id;
#endif
            }
            return translation;

        }
    }
}
