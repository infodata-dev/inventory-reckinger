﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Inventory.Droid.Services;
using Inventory.Services.DependencyServices;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xamarin.Forms;

[assembly: Dependency(typeof(SqLiteService))]
namespace Inventory.Droid.Services
{
    public class SqLiteService : ISqLiteService
    {
        public SQLiteConnection GetConnexion(string databaseName)
        {
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var path = Path.Combine(documentsPath, databaseName);
            var conn = new SQLiteConnection(path);
            return conn;
        }

        public SQLiteAsyncConnection GetAsyncConnexion(string databaseName)
        {
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var path = Path.Combine(documentsPath, databaseName);
            var conn = new SQLiteAsyncConnection(path);
            return conn;
        }
    }
}