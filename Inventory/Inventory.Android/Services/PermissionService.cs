﻿using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidX.Core.Content;
using Inventory.Droid.Services;
using Inventory.Services.DependencyServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

//[assembly: Dependency(typeof(DeviceInfoService))]
namespace Inventory.Droid.Services
{
    public class PermissionService
    {
        public bool CheckReadExternalStoragePermission()
        { 
           return CheckReadPermissionGranted(Manifest.Permission.ReadExternalStorage);
        }
        private bool CheckReadPermissionGranted(string Permissions)
        {
            if (ContextCompat.CheckSelfPermission(Android.App.Application.Context, Manifest.Permission.ReadExternalStorage) != Permission.Granted)
                return false;
            else
                return true;
        }


        public void RequestReadExternalStoragePermission()
        {
            var currentContext = MainActivity.Standalone;
            if (currentContext.ShouldShowRequestPermissionRationale(Manifest.Permission.ReadExternalStorage))
                currentContext.RequestPermissions(new string[] { Manifest.Permission.ReadExternalStorage }, 1);
            else
                currentContext.RequestPermissions(new string[] { Manifest.Permission.ReadExternalStorage }, 1);
        }

        public bool CheckWriteExternalStoragePermission()
        {
            return CheckWritePermissionGranted(Manifest.Permission.WriteExternalStorage);
        }
        private bool CheckWritePermissionGranted(string Permissions)
        {
            if (ContextCompat.CheckSelfPermission(Android.App.Application.Context, Manifest.Permission.WriteExternalStorage) != Permission.Granted)
                return false;
            else
                return true;
        }


        public void RequestWriteExternalStoragePermission()
        {
            var currentContext = MainActivity.Standalone;
            if (currentContext.ShouldShowRequestPermissionRationale(Manifest.Permission.WriteExternalStorage))
                currentContext.RequestPermissions(new string[] { Manifest.Permission.WriteExternalStorage }, 1);
            else
                currentContext.RequestPermissions(new string[] { Manifest.Permission.WriteExternalStorage }, 1);
        }
    }
}