﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Inventory.Droid.Services;
using Inventory.Services.DependencyServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xamarin.Forms;

[assembly: Dependency(typeof(ModeKioskService))]
namespace Inventory.Droid.Services
{
    class ModeKioskService : IModeKioskService
    {
        public async void SwitchModeKiosk()
        {
            PermissionService permission = new PermissionService();
            if (!permission.CheckWriteExternalStoragePermission())
            {
                permission.RequestWriteExternalStoragePermission();
            }
            if (!permission.CheckReadExternalStoragePermission())
            {
               permission.RequestReadExternalStoragePermission();
            }
            if (permission.CheckWriteExternalStoragePermission() && permission.CheckReadExternalStoragePermission())
            {
                string path = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads).ToString();
                var oldFile = path + "/KioskApp.txt";
                if (File.Exists(oldFile))
                {
                    File.Move(oldFile, path + "/KioskApp2.txt");
                }
                else
                {
                    oldFile = path + "/KioskApp2.txt";
                    if (File.Exists(oldFile))
                    {
                        File.Move(oldFile, path + "/KioskApp.txt");
                    }
                }
            }
            return;
        }

        public bool GetSwitchModeKiosk()
        {
            PermissionService permission = new PermissionService();
            if (!permission.CheckWriteExternalStoragePermission())
            {
                permission.RequestWriteExternalStoragePermission();
            }
            if (!permission.CheckReadExternalStoragePermission())
            {
                permission.RequestReadExternalStoragePermission();
            }
            if (permission.CheckWriteExternalStoragePermission() && permission.CheckReadExternalStoragePermission())
            {
                string path = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads).ToString();
                var oldFile = path + "/KioskApp.txt";
                if (File.Exists(oldFile))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
    }
}