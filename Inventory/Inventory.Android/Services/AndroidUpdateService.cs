﻿using Android;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using AndroidX.Core.Content;
using Inventory.Droid.Services;
using Inventory.Models;
using Inventory.Resources;
using Inventory.Services.DependencyServices;
using Inventory.Services.Web;
using Java.IO;
using Java.Net;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(AndroidUpdateService))]
namespace Inventory.Droid.Services
{
	public class AndroidUpdateService : IUpdateService
	{
        /// <summary>
        /// Download and execute an apk
        /// </summary>
        /// <param name="token">Cancellation token</param>
        /// <param name="apkPath">Apk path</param>
        /// <returns>WSResult</returns>
        public async Task<WSResult> DownloadAndUpdateApp(CancellationToken token, string apkPath)
        {
            return await Task<WSResult>.Run(() =>
            {
                try
                {
                    // Check permissions
                    if (Android.App.Application.Context.CheckSelfPermission(Manifest.Permission.WriteExternalStorage) != (int)Permission.Granted)
                        MainActivity.Standalone.RequestPermissions(new string[] { Manifest.Permission.WriteExternalStorage }, 0);

                    if (Android.App.Application.Context.CheckSelfPermission(Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
                        MainActivity.Standalone.RequestPermissions(new string[] { Manifest.Permission.ReadExternalStorage }, 0);

                    URL url = new URL(apkPath);
                    HttpURLConnection httpConnection = (HttpURLConnection)url.OpenConnection();
                    httpConnection.RequestMethod = "GET";
                    httpConnection.DoOutput = true;
                    httpConnection.Connect();

                    string path = Android.OS.Environment.ExternalStorageDirectory.Path + "/";
                    Java.IO.File outputFile = new Java.IO.File(path + "update.apk");
                    if (outputFile.Exists())
                        outputFile.Delete();

                    outputFile.CreateNewFile();

                    // File stream
                    FileOutputStream fos = new FileOutputStream(outputFile);
                    Stream stream = (Stream)httpConnection.InputStream;
                    byte[] buffer = new byte[1024];
                    int i = 0;
                    while ((i = stream.Read(buffer)) != 0)
                    {
                        token.ThrowIfCancellationRequested();
                        fos.Write(buffer, 0, i);
                    }
                    fos.Close();
                    stream.Close();

                    // Launch activity
                    Intent intent = new Intent(Intent.ActionView);
                    intent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
                    if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.N)
                    {
                        Android.Net.Uri apkURI = FileProvider.GetUriForFile(Android.App.Application.Context,
                            Android.App.Application.Context.PackageName + ".fileprovider", outputFile);
                        intent.SetDataAndType(apkURI, "application/vnd.android.package-archive");
                        intent.AddFlags(ActivityFlags.GrantReadUriPermission);
                    }
                    else
                    {
                        Android.Net.Uri u = Android.Net.Uri.FromFile(outputFile);
                        intent.SetDataAndType(u, "application/vnd.android.package-archive");
                    }
                    Android.App.Application.Context.StartActivity(intent);
                    
                    return new WSResult(true);
                }
                catch (Exception exc)
                {
                    return new WSResult(exc.Message);
                }
            },
            token);
        }
    }
}